#!/bin/bash

# get path of ASD files
asddir=$(python -c \
"try:
    import importlib.resources as importlib_resources
except ImportError:  # python < 3.7
    import importlib_resources
with importlib_resources.path('inspiral_range.test', 'O2.txt') as p:
    print(str(p.parent))
")

# module execution
python -m inspiral_range --asd ${asddir}/O2.txt

# entry point
inspiral-range --asd ${asddir}/O2.txt

# O3 ASD with 30/30 BBH
inspiral-range --asd ${asddir}/O3.txt m1=30 m2=30
