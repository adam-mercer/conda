#!/usr/bin/env python

"""Run integration tests for python-lalsimulation
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import lalsimulation
from numpy import isclose


def test_sim_noise():
    assert isclose(
        lalsimulation.SimNoisePSDaLIGOZeroDetHighPower(100),
        1.5232929254447294e-47,
    )
