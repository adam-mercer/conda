#!/usr/bin/env python

"""Publish the conda environments to a given location
"""

import argparse
import json
import logging
import os
import re
import subprocess
import sys
import warnings
from distutils.spawn import find_executable
from pathlib import Path
from unittest import mock

import yaml

from utils import create_logger

# find conda on PATH, or relative to the prefix
CONDA = find_executable('conda')
_rel_conda = Path(sys.prefix) / "condabin" / "conda"
if CONDA is None and _rel_conda.is_file():
    CONDA = str(_rel_conda)

STABLE_ENV = re.compile(r".*-py\d\d(.yaml)?\Z")

# configure logging
logger = create_logger("build")


# -- utilities ----------------------------------

def _env_name(filename):
    with open(filename, "r") as fobj:
        return yaml.safe_load(fobj)["name"]


def _get_env_path():
    try:
        paths = os.environ["CONDA_ENVS_PATH"].split(os.pathsep)
    except KeyError:
        # use config
        out = subprocess.check_output([
            CONDA,
            "config",
            "--get",
            "envs_dirs",
            "--json"
        ]).decode("utf-8").strip()
        try:
            paths = json.loads(out).get("get", {})["envs_dirs"]
        except KeyError:
            # get root and user dirs and sort appropriately
            rootenvs = Path(subprocess.check_output([
                CONDA,
                "info",
                "--base",
            ]).decode("utf-8").strip()) / "envs"
            userenvs = (Path("~") / ".conda").expanduser()
            if os.access(rootenvs.parent, os.W_OK):
                paths = (rootenvs, userenvs)
            else:
                paths = (userenvs, rootenvs)

    for path in map(Path, paths):
        if os.access(path, os.W_OK):
            return path
    raise RuntimeError(
        "cannot determine conda envs path",
    )


def list_envs(**kwargs):
    envs = json.loads(call_conda(
        "env",
        "list",
        "--json",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        **kwargs,
    )[0])["envs"]
    envdict = {}
    for path in map(Path, envs):
        name = path.name
        if name in {"_build_env"}:
            continue
        envdict[name] = path
    return envdict


# -- conda interactions -------------------------


def _name_or_prefix_args(name=None, prefix=None, required=False):
    if required and not (name or prefix):
        raise ValueError(
            "name or prefix must be given",
        )
    if name:
        return ("--name", str(name))
    if prefix:
        return ("--prefix", str(prefix))
    return ()


def call_conda(*args, **kwargs):
    cmd = [CONDA] + list(args)
    cmdstr = " ".join(cmd)
    if kwargs.get("logger"):
        kwargs.pop("logger").debug("$ {}".format(cmdstr))
    proc = subprocess.Popen(cmd, **kwargs)
    comms = proc.communicate()
    if proc.returncode:
        raise subprocess.CalledProcessError(proc.returncode, cmdstr)
    return comms


def remove_environment(name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix, required=True)
    return call_conda(
        "env",
        "remove",
        "--quiet",
        *args,
        **kwargs,
    )


def create_environment(filename, name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix)
    return call_conda(
        "env",
        "create",
        "--quiet",
        "--file", str(filename),
        *args,
        **kwargs,
    )


def update_environment(filename, name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix)
    return call_conda(
        "env",
        "update",
        "--file", str(filename),
        *args,
        **kwargs,
    )


def update_or_replace_environment(filename):
    try:
        return update_environment(filename)
    except subprocess.CalledProcessError as exc:
        warnings.warn("failed to update environment")
        try:
            remove_environment(filename)
            return create_environment(filename)
        except subprocess.CalledProcessError:
            pass
        raise


# -- main ---------------------------------------

DEFAULT_ENV_PATH = _get_env_path()

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "environments_dir",
    type=Path,
    help="path to directory containing environments",
)
parser.add_argument(
    "-c",
    "--conda",
    default=CONDA,
    required=CONDA is None,
    help="path to conda executable (default: %(default)s)",
)
parser.add_argument(
    "-t",
    "--tag",
    help="build the given dated tag",
)
parser.add_argument(
    "-e",
    "--envs-dir",
    type=Path,
    default=DEFAULT_ENV_PATH,
    required=DEFAULT_ENV_PATH is None,
    help="directory under which to create all environments",
)
parser.add_argument(
    "-d",
    "--dry-run",
    action="store_true",
    default=False,
    help="just print what would be done, don't actually do anything",
)
parser.add_argument(
    "--skip-proposed",
    action="store_true",
    default=False,
    help="skip building proposed environments",
)
parser.add_argument(
    "--skip-testing",
    action="store_true",
    default=False,
    help="skip building testing environments",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    default=False,
    help="print verbose output",
)

args = parser.parse_args()
CONDA = args.conda

if args.dry_run:
    # if asked for a dry-run, mock out popen to do nothing,
    # this way we test almost everything
    _patch = mock.patch("subprocess.Popen", returncode=0)
    mockedpopen = _patch.start().return_value
    mockedpopen.communicate.return_value = b"{\"envs\": []}", b"{}"
    mockedpopen.returncode = 0

if args.verbose:
    logger.setLevel(logging.DEBUG)

args.envs_dir = args.envs_dir.resolve(strict=False)
if not args.dry_run:
    args.envs_dir.mkdir(parents=True, exist_ok=True)
    args.envs_dir = args.envs_dir.resolve(strict=True)

# -- run

startdir = Path.cwd()

# find environments to build
envs = args.environments_dir.rglob("*.yaml")

# list existing environments
existing = list_envs(logger=logger)
logger.info("{} existing environments found".format(len(existing)))

# record new/updated environments
ours = {}
symlinks = {}

for env in envs:
    name = _env_name(env)

    logger.info("-- Processing {0: <10} ----------".format(name))

    # only build tagged stable environments
    isstable = STABLE_ENV.match(str(env)) is not None
    if isstable and not args.tag:
        logger.debug("skipping (no --tag)")
        continue
    elif args.skip_proposed and name.endswith("-proposed"):
        logger.debug(f"skipping {name} (--skip-proposed)")
        continue
    elif args.skip_testing and name.endswith("-testing"):
        logger.debug(f"skipping {name} (--skip-testing)")
        continue
    elif isstable:
        envname = "{}-{}".format(name, args.tag)
        logger.debug("environment will be created as {}".format(envname))
    else:
        envname = name

    prefix = args.envs_dir / envname

    # if environment exists, try and update it
    if existing.get("envname") == prefix:
        update_or_replace_environment(
            env,
            prefix=prefix,
            logger=logger,
        )
        logger.debug("updated environment")
    else:
        create_environment(
            env,
            prefix=prefix,
            logger=logger,
        )
        logger.debug("created new environment")

    ours[envname] = prefix

    # create or update symlink for tagged stable
    if isstable:
        # e.g. gw-py27 -> gw-py27-YYYYMMDD
        link = prefix.parent / name
        if not args.dry_run:
            os.chdir(prefix.parent)
            pname = Path(name)
            if pname.is_symlink():
                pname.unlink()
            pname.symlink_to(prefix.name)
            os.chdir(startdir)
        symlinks[name] = (link, prefix)
        logger.debug("linked {} -> {}".format(name, envname))
    logger.info("complete".format(envname))

logger.info("-- All environments created/updated")
if not args.dry_run:
    logger.debug("The following environments were created/updated:")
    for name, prefix in ours.items():
        logger.debug(f"{name!r}: {prefix}")
    if not ours:
        logger.debug("[EMPTY]")
    logger.debug("The following symbolic links were created:")
    for name, (link, target) in symlinks.items():
        logger.debug(f"{name!r}: {link} -> {target}")
    if not symlinks:
        logger.debug("[EMPTY]")
