#!/bin/bash
#
# Create a new stable release of the IGWN Conda Distribution,
# based on today's date
#

# echo a command to the shell as "$ <command>" and then run it
echo_and_execute() {
    echo "$ $@"
    "$@"
}

tagdate=$(date +"%Y%m%d")
datestr=$(date +"%d %B %Y")
tagnum=$(git tag --list ${tagdate}.* | wc -l)
if [ ${tagnum} -eq 0 ]; then
    tag="${tagdate}"
else
    tag="${tagdate}.${tagnum}"
fi
echo "Tagging ${tag}"

echo "-- Checking out (up-to-date) master branch"
echo_and_execute git fetch origin
echo_and_execute git checkout master
echo_and_execute git pull
echo "-- Tagging"
echo_and_execute git tag --sign ${tag} -m "IGWN Conda stable release for ${datestr}"
echo "-- Pushing"
echo_and_execute git push origin ${tag} --signed=if-asked --verbose --verify
