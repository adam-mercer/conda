#!/usr/bin/env python3

import logging

LOGGER_FORMAT = "[%(asctime)-15s] %(name)s | %(levelname)+8s | %(message)s"
LOGGER_DATEFMT = "%Y-%m-%d %H:%M:%S"

try:
    from coloredlogs import (
        ColoredFormatter,
        install as install_coloredlogs,
    )
except ImportError:
    ColoredFormatter = logging.Formatter
else:
    install_coloredlogs(
        fmt=LOGGER_FORMAT,
        datefmt=LOGGER_DATEFMT,
    )


class Logger(logging.Logger):
    """`~logging.Logger` with a nice format
    """
    def __init__(self, name, level=logging.DEBUG):
        super().__init__(name, level=level)
        colorformatter = ColoredFormatter(
            fmt=LOGGER_FORMAT,
            datefmt=LOGGER_DATEFMT,
        )
        console = logging.StreamHandler()
        console.setFormatter(colorformatter)
        self.addHandler(console)


def create_logger(name, level=logging.INFO):
    """Configure a logger
    """
    return Logger(f"gwconda | {name}", level=level)
